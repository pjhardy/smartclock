const int max_message_length = 256;
int serialIndex = 0;
char serialBuf[max_message_length];

void parseSerial() {
  char *s = serialBuf;
  int topic_idx = atol(strtok(s, "|"));
  switch (topic_idx) {
  case 0: // weather/forecast
    char *minbuf;
    char *maxbuf;
    minbuf = strtok(NULL, "|");
    maxbuf = strtok(NULL, "|");
    snprintf(forecast_temps, 6, "%s-%s", minbuf, maxbuf);
    iconidx = atol(strtok(NULL, "|"));
    char *forecastbuf;
    forecastbuf = strtok(NULL, "|");
    // Get rid of the CR at the end
    forecastbuf[strlen(forecastbuf)-1]='\0';
    snprintf(forecast, 32, "%s", forecastbuf);
    break;
  case 1: // edgar/out/4/4 (temperature readings from outdoor sensor)
    strtok(NULL, "|"); strtok(NULL, "|"); // Next two fields unneeded
    char *buf;
    buf = strtok(NULL, "|");
    // Get rid of the CR at the end
    buf[strlen(buf)-1]='\0';
    snprintf(current_temp, 7, "%s\xb0", buf);
    break;
  case 2: // edgar/out/4/5 (humidity readings from outdoor sensor)
    // Nothing yet
    break;
  case 3: // transport/uber (Uber time estimate)
    char *product_buf;
    product_buf = strtok(NULL, "|");
    snprintf(uber_product, 8, "%s", product_buf);
    uber_time = atol(strtok(NULL, "|"));
  default:
    // Don't care
    break;
  }
}

void readSerial() {
  while (Serial.available()) {
    char serialByte = Serial.read();
    if (serialByte != '\n' && serialIndex < (max_message_length-1)) {
      serialBuf[serialIndex] = serialByte;
      serialIndex++;
    }
    if (serialByte == '\n') {
      serialBuf[serialIndex] = '\0';
      parseSerial();
      serialIndex = 0;
    }
    if (serialIndex >= max_message_length) {
      // Giving up
      serialIndex = 0;
    }
  }
}
