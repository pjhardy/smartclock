const int icon_y_offset = 32;

const unsigned char sunny_day[] U8G_PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00,
  0x00, 0xc0, 0x00, 0x00, 0x20, 0xc0, 0x00, 0x01, 0x70, 0x00, 0x80, 0x03,
  0xe0, 0x00, 0xc0, 0x01, 0xc0, 0x01, 0xe0, 0x00, 0x80, 0xf0, 0x43, 0x00,
  0x00, 0xf8, 0x07, 0x00, 0x00, 0x1c, 0x0e, 0x00, 0x00, 0x0e, 0x1c, 0x00,
  0x00, 0x06, 0x18, 0x00, 0x00, 0x03, 0x30, 0x00, 0x1e, 0x03, 0x30, 0x1e,
  0x1e, 0x03, 0x30, 0x1e, 0x00, 0x03, 0x30, 0x00, 0x00, 0x06, 0x18, 0x00,
  0x00, 0x0e, 0x1c, 0x00, 0x00, 0x1c, 0x0e, 0x00, 0x00, 0xf8, 0x07, 0x00,
  0x80, 0xe0, 0x41, 0x00, 0xc0, 0x01, 0xe0, 0x00, 0xe0, 0x00, 0xc0, 0x01,
  0x70, 0x00, 0x80, 0x03, 0x20, 0xc0, 0x00, 0x01, 0x00, 0xc0, 0x00, 0x00,
  0x00, 0xc0, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

const unsigned char clear[] U8G_PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0xfc, 0x03, 0x00, 0x00, 0xff, 0x03, 0x00,
  0x80, 0xff, 0x03, 0x00, 0xe0, 0x87, 0x03, 0x00, 0xf0, 0x83, 0x03, 0x00,
  0xf0, 0x80, 0x07, 0x00, 0x78, 0x00, 0x07, 0x00, 0x78, 0x00, 0x0f, 0x00,
  0x3c, 0x00, 0x1e, 0x00, 0x3c, 0x00, 0x3e, 0x00, 0x1e, 0x00, 0xfc, 0x00,
  0x1e, 0x00, 0xf8, 0x1f, 0x1e, 0x00, 0xe0, 0x1f, 0x1e, 0x00, 0x80, 0x1f,
  0x1e, 0x00, 0x00, 0x1c, 0x1e, 0x00, 0x00, 0x1c, 0x1e, 0x00, 0x00, 0x1e,
  0x1c, 0x00, 0x00, 0x0e, 0x3c, 0x00, 0x00, 0x0f, 0x3c, 0x00, 0x00, 0x0f,
  0x78, 0x00, 0x80, 0x07, 0xf8, 0x00, 0xc0, 0x07, 0xf0, 0x01, 0xe0, 0x03,
  0xe0, 0x07, 0xf8, 0x01, 0xc0, 0xff, 0xff, 0x00, 0x00, 0xff, 0x3f, 0x00,
  0x00, 0xfe, 0x1f, 0x00, 0x00, 0xf8, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00 };

const unsigned char unknown[] U8G_PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00,
  0x00, 0xf8, 0x01, 0x00, 0x00, 0xfc, 0x03, 0x00, 0x00, 0xf6, 0x07, 0x00,
  0x00, 0xf7, 0x0e, 0x00, 0x00, 0xf7, 0x0e, 0x00, 0x00, 0xf7, 0x0e, 0x00,
  0x00, 0xf7, 0x0e, 0x00, 0x00, 0xf0, 0x0e, 0x00, 0x00, 0x60, 0x06, 0x00,
  0x00, 0x60, 0x07, 0x00, 0x00, 0x60, 0x03, 0x00, 0x00, 0xe0, 0x01, 0x00,
  0x00, 0xe0, 0x01, 0x00, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00,
  0x00, 0x60, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00,
  0x00, 0xf0, 0x00, 0x00, 0x00, 0xf0, 0x00, 0x00, 0x00, 0xf0, 0x00, 0x00,
  0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

const int num_forecasts = 19;

const unsigned char* day_icons[num_forecasts] = {
  sunny_day, // 1: Sunny
  clear,   // 2: Clear
  unknown,   // 3: Mostly sunny / Partly cloudy
  unknown,   // 4: Cloudy
  unknown,   // 5: Hazy
  unknown,   // 6: Light rain
  unknown,   // 7: unused
  unknown,   // 8: Light rain
  unknown,   // 9: Windy
  unknown,   // 10: Fog
  unknown,   // 11: Shower
  unknown,   // 12: Rain
  unknown,   // 13: Dusty
  unknown,   // 14: Frost
  unknown,   // 15: Snow
  unknown,   // 16: Storm
  unknown,   // 17: Light shower
  unknown,   // 18: Heavy shower
  unknown,   // 19: Cyclone
};

const unsigned char* night_icons[num_forecasts] = {
  sunny_day, // 1: Sunny
  clear,   // 2: Clear
  unknown,   // 3: Mostly sunny / Partly cloudy
  unknown,   // 4: Cloudy
  unknown,   // 5: Hazy
  unknown,   // 6: Light rain
  unknown,   // 7: unused
  unknown,   // 8: Light rain
  unknown,   // 9: Windy
  unknown,   // 10: Fog
  unknown,   // 11: Shower
  unknown,   // 12: Rain
  unknown,   // 13: Dusty
  unknown,   // 14: Frost
  unknown,   // 15: Snow
  unknown,   // 16: Storm
  unknown,   // 17: Light shower
  unknown,   // 18: Heavy shower
  unknown,   // 19: Cyclone
};

void draw_forecast_icon() {
  if (iconidx > 19 || iconidx < 1) {
    iconidx = 7;
  }
  display->drawXBMP(0, icon_y_offset, icon_width, icon_height, day_icons[iconidx-1]);
}

const void forecast_message_display() {
  display->setFont(u8g_font_helvR08);
  display->drawStr(64, 50, forecast);
}
