/* SmartClock
   A sketch for driving a nice-looking clock.

   Hardware:
   * Arduino Uno or compatible.
   * 128x64 graphic LCD using ST7565 controller.
   * DS1307 RTC module.
   * LDR. Any cadmium sulfide based sensor will work for this.

   Libraries:
   * u8glib display library: https://github.com/olikraus/u8glib
   * Adafruit RTClib: https://github.com/adafruit/RTClib
*/

#include <Wire.h>

#include <U8glib.h>
#include <RTClib.h>

U8GLIB_LM6059* display = NULL; // An object to control the LCD.

// pin allocations
const uint8_t backlightR=3; // Pin the red backlight LED is attached to.
const uint8_t backlightG=5; // Pin the green backlight LED is attached to.
const uint8_t backlightB=6; // Pin the blue backlight LED is attached to.
const uint8_t ldrPin=A3; // Pin the LDR is attached to.

RTC_DS1307 RTC; // An object to interact with the real-time clock.

DateTime now; // This variable will always hold the current time.
char timebuf[6]; // A string for the hours and minutes display.
char secbuf[3]; // A string for the seconds display.
char datebuf[6]; // A string for the date display.

char forecast[32]; // A string for holding the current forecast
char forecast_temps[6] = "00-00"; // A string for the max/min forecast temps
char current_temp[7] = "00.0\xb0"; // A string for the current temperature
int iconidx = 7; // An int for the index of the forecast icon. 7 is unused.
char uber_product[8] = "uberX"; // String for the name of the Uber product
uint8_t uber_time = 0; // Uber time estimate

const long message_persist_time = 5000; // How long to keep a message on screen
unsigned long message_time = 0; // Tracking messages on the screen

const uint8_t num_messages = 4; // How many message functions exist
uint8_t message_idx = 0; // Index of the current message

const int icon_width = 30;
const int icon_height = 30;

// The RTC returns day of week as an int. This is just a handy
// array of day names, the int from the RTC can be used as an
// index to get the short day name.
const char *daysofweek[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

// Sample the brightness, return a number between 0-255.
// 0 is peak dark, 255 is hopefully equivalent to a sunny day
// in my lounge room.
uint8_t measureBrightness() {
  int raw = analogRead(ldrPin);
  // I think a max reading of 200 is reasonable, based on
  // shining lamp light directly on the sensor in my spare
  // room at midnight.
  raw = min(raw, 200);
  // These mappings probably look weird:
  // Currently brightness of the LEDs is 255 - our return.
  // A brightness higher than 240 or so washes out the display.
  // A brightness lower than about 10 is unreadably dim.
  return constrain(map(raw, 0, 200, 15, 240), 0, 240);
}

// Currently a dumb function that just sets lights white
// with varying brightness based on ambient light.
void setLights() {
  uint8_t backlight = measureBrightness();

  analogWrite(backlightR, 255-backlight);
  analogWrite(backlightG, 255-backlight);
  analogWrite(backlightB, 255-backlight);
}

// Main LCD draw routine
void draw() {
  snprintf(timebuf, 6, "%02i:%02i", now.hour(), now.minute());
  snprintf(secbuf, 3, "%02i", now.second());
  snprintf(datebuf, 6, "%02i/%02i", now.day(), now.month());

  display->setFont(u8g_font_fur30n);
  display->drawStr(0, 33, timebuf);
  // Still have a minor bug in the time display: times < 10am are
  // rendered as, eg, "8:00" without a leading space, pushing
  // everything over to the left. Measuring the length of the timebuf
  // at least makes sure that the other elements are still hard against
  // the edge of the time.
  uint8_t timewidth = display->getStrWidth(timebuf) + 2;
  display->setFont(u8g_font_fur11);
  display->drawStr(timewidth, 13, secbuf);

  display->setFont(u8g_font_helvR08);
  display->drawStr(timewidth, 23, daysofweek[now.dayOfTheWeek()]);
  display->drawStr(timewidth, 33, datebuf);

  draw_forecast_icon();

  display->setFont(u8g_font_fur11);
  display->drawStr(31, 48, current_temp);
  display->setFont(u8g_font_helvR08);
  display->drawStr(31, 58, forecast_temps);

  // Exec the appropriate message_functions display function
  switch(message_idx) {
  case 0:
    forecast_message_display();
    break;
  case 1:
    uber_message_display();
    break;
  case 2:
    train_message_display();
    break;
  case 3:
    bus_message_display();
  }
}

void setup() {
  pinMode(backlightR, OUTPUT);
  pinMode(backlightG, OUTPUT);
  pinMode(backlightB, OUTPUT);
  setLights();

  // Stop pin 13 from floating so the LED doesn't flicker
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);

  // pin 12 - Serial data out (SID)
  // pin 11 - Serial clock out (SCLK)
  // pin 10 - Data/Command select (RS or A0)
  // pin 9  - LCD reset (RST)
  // pin 8  - LCD chip select (CS)
  display = new U8GLIB_LM6059(11, 12, 8, 10, 9);

  Serial.begin(115200);
  Wire.begin();
  RTC.begin();

  // Uncomment to set the RTC time to this computer's time at compile
  //RTC.adjust(DateTime(__DATE__, __TIME__));
}

void loop() {
  // Retrieve current time from the RTC
  now = RTC.now();
  unsigned long now_millis = millis();
  // Sample ambient light and update LCD backlight
  setLights();
  // Check for serial data
  readSerial();
  // Make sure we're displaying the right message
  if (now_millis > message_time) {
    message_time = now_millis + message_persist_time;
    message_idx++;
    if (message_idx == num_messages) {
      message_idx = 0;
    }
  }
  // Update the display
  display->firstPage();
  do {
    draw();
  } while (display->nextPage());
}
