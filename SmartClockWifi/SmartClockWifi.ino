/* SmartClockWifi
   The ESP8266 component of my smart clock.
   Joins an MQTT service and subscribes to interesting info topics.

   Hardware:
   * One ESP8266 wifi module, running the current arduino core.

   Libraries:
   * PubSubClient: http://pubsubclient.knolleary.net/

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid="Router? I hardly knew 'er! 2.4"; // WiFi ssid
const char* password="The reaper does not listen to the harvest."; // WiFi password

const char* mqtt_server="10.17.17.3"; // MQTT Server address
const char* mqtt_clientid="SmartClock"; // MQTT client ID
const char* mqtt_username="arduino"; // MQTT username
const char* mqtt_password="Ziu7im2i"; // MQTT password

// Topic for system-level announcements about this client
const char* announce_topic="smartclock/announce";

// List of topics that we want to subscribe to
const int num_subscribe_topics = 6;
const char *subscribe_topics[] = {
  "weather/forecast", // Weather forecast script running on sara
  "edgar/out/4/4",    // Outdoor temperature sensor
  "edgar/out/4/5",    // Outdoor humidity sensor
  "transport/uber",   // Arrival estimates from Uber
  "transport/train/martin_place", // Trains to Martin Place
  "transport/bus/422/city" // The 422 bus to the city
};

const int max_message_length = 256; // Maximum serial message size

WiFiClient espClient;
PubSubClient mqttClient(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup_wifi() {
  delay(10);
  WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
}

void reconnect() {
  while (!mqttClient.connected()) {
    if (mqttClient.connect(mqtt_clientid, mqtt_username, mqtt_password)) {
      mqttClient.publish(announce_topic, "Smart clock online");
      for (int i=0; i<num_subscribe_topics; i++) {
        mqttClient.subscribe(subscribe_topics[i]);
      }
    } else {
      delay(500);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  payload[length] = '\0';
  bool found=false;
  int i;
  for (i=0; i<num_subscribe_topics; i++) {
    if (strcmp(topic, subscribe_topics[i]) == 0) {
      found = true;
      break;
    }
  }
  if (!found) {
    i++;
  }
  char buf[max_message_length];
  snprintf(buf, max_message_length, "%i|%s", i, payload);
  Serial.println(buf);
}

void setup() {
  Serial.begin(115200);
  mqttClient.setServer(mqtt_server, 1883);
  mqttClient.setCallback(callback);
  setup_wifi();
}

void loop() {
  if (!mqttClient.connected()) {
    reconnect();
  }
  mqttClient.loop();
}
